jQuery(function ($){

    const editOwner = $('#editOwner');
    if (editOwner.validate) {
        editOwner.validate({
            rules: {
                vat: {
                    required: true,
                    digits: true,
                    minlength: 7,
                    maxlength: 7
                },

                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },

                address: {
                    required: true
                },

                password:{
                    required: true
                },

                phoneNumber: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10
                },

                propertyType:{
                    required: true
                }

            },
            messages: {
                vat: {
                    required: 'Enter your Password broo! 🤬',
                    minlength: 'Vat should be more than 7 digits',
                    maxlength: 'Vat should be maxed out at 7 digits',
                    digits: 'Password should contain only digits'
                },

                email: {
                    required: 'Enter your email honey! 📧',
                },
                firstName: {
                    required: 'Enter your first name'
                },
                lastName: {
                    required: 'Enter your last name'
                },
                password: {
                    required: 'Enter your password'
                },
                phoneNumber: {
                    required: 'Enter your phone',
                    minlength: 'Phone number should be more than 10 digits',
                    maxlength: 'Phone number should be maxed out at 10 digits',
                    digits: 'Phone number should contain only digits'
                },
                propertyType: {
                    required: 'Enter your property type'
                },
                address: {
                    required: 'Enter your address'
                }
            }
        });
    }




    const $searchByVatOwner = $('#searchByVatOwner');
    if ($searchByVatOwner.validate) {
        $searchByVatOwner.validate({
            rules: {
                vat: {
                    required: true,
                    digits: true,
                    minlength: 7,
                    maxlength: 7
                },
            },
            messages: {
                vat: {
                    required: "Vat is required!"
                }
            }
        })
    }

    const $searchByEmail = $('#searchByEmail');
    if ($searchByEmail.validate) {
        $searchByEmail.validate({
            rules: {
                email: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: 'Enter your email honey! 📧',
                }
            }
        })
    }
});