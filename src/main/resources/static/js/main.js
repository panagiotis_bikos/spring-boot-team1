jQuery(function ($){

    const $createRepairForm = $('#createRepairForm');
    if ($createRepairForm.validate) {
        $createRepairForm.validate({
            rules: {
                repairId: {
                    required: true,
                    digits: true
                },
                repairDate: {
                    required: true
                },
                repairCost:{
                    required:true,
                    digits: true
                },
                repairAddress: {
                    required:true
                },
                ownerId: {
                    required:true,
                    digits: true,
                    minlength: 7 ,
                    maxlength: 7
                }
            },
            messages: {
                repairId: {
                    required: "Repair Id is required.",
                    digits: "Only digits accepted."
                },
                repairDate: {
                    required: "Date is required."
                },
                repairCost: {
                    required: "Cost is required.",
                    digits: "Only digits accepted."
                },
                repairAddress: {
                  required: "Address is required."
                },
                ownerId: {
                    minlength:"It should not be less than 7 digits",
                    maxlength:"It should be maxed out at 7 digits",
                    required: "Owner Id is required."
                }
            }
        });
    }

    const $searchByDate = $('#searchByDate');
    if ($searchByDate.validate) {
        $searchByDate.validate({
            rules: {
                repairDate: {
                    required: true
                }
            },
            messages: {
                repairDate: {
                    required: "Date is required."
                }
            }
        })
        }

    const $searchByPeriod = $('#searchByPeriod');
    if ($searchByPeriod.validate) {
        $searchByPeriod.validate({
            rules: {
                repairDate: {
                    required: true
                },
                repairDate2: {
                    required: true
                }
            },
            messages: {
                repairDate: {
                    required: "Date is required."
                },
                repairDate2: {
                    required: "Date is required."
                }
            }
        })
    }

    const $searchByVat = $('#searchByVat');
    if ($searchByVat.validate) {
        $searchByVat.validate({
            rules: {
                ownerId: {
                    required: true,
                    digits: true,
                    minlength:7,
                    maxlength:7
                }
            },
            messages: {
                ownerId: {
                    required: "Vat is required.",
                    minlength:"It should not be less than 7 digits",
                    maxlength: "It should be maxed out at 7 digits"
                }
            }
        })
    }

    const $editRepair = $('#editRepair');
    if ($editRepair.validate) {
        $editRepair.validate({
            rules: {
                date: {
                    required: true
                },
                statusType:{
                    required:true,
                },
                repairType: {
                    required:true
                },
                cost:{
                    required:true,
                    digits: true
                },
                address: {
                    required:true
                }
            },
            messages: {
                repairId: {
                    required: "Repair Id is required.",
                },
                repairDate: {
                    required: "Date is required."
                },
                repairCost: {
                    required: "Cost is required.",
                    digits: "Only digits accepted."
                },
                repairAddress: {
                    required: "Address is required."
                },
                ownerId: {
                    required: "Owner Id is required.",
                    minlength:"It should not be less than 7 digits.",
                    maxlength: "It should be maxed out at 7 digits."
                }
            }
        });
    }
});