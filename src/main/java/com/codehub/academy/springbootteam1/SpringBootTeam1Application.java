package com.codehub.academy.springbootteam1;

import com.codehub.academy.springbootteam1.repository.PropertyOwnerRepository;
import com.codehub.academy.springbootteam1.service.PropertyOwnerService;
import com.codehub.academy.springbootteam1.service.RepairService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.ParseException;

@SpringBootApplication
public class SpringBootTeam1Application implements CommandLineRunner {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PropertyOwnerService propertyOwnerService;


	@Autowired
	private RepairService repairService;

	@Autowired
	private PropertyOwnerRepository propertyOwnerRepository;


	public static void main(String[] args) {
		SpringApplication.run(SpringBootTeam1Application.class, args);
	}

	@Override
	public void run(String... args) throws ParseException {

//		logger.info("=============================");
//		logger.info("====Finding Owner by Vat====");
//		Optional<PropertyOwner> foundOwner = propertyOwnerService.findPropertyOwnerByVat(1354123L);
//		foundOwner.ifPresent(owner -> logger.info(owner.toString()));
//		logger.info("=============================");
//		logger.info("=============================");
//
//		logger.info("=============================");
//		logger.info("====Finding Owner by Email====");
//		Optional<PropertyOwner> foundOwner2 = propertyOwnerService.findPropertyOwnerByEmail("pap@hotmail.com");
//		foundOwner2.ifPresent(owner -> logger.info(owner.toString()));
//		logger.info("=============================");
//		logger.info("=============================");


//
//
//		logger.info("=============================");
//		logger.info("====Finding Repair By Date====");
//
//		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//		LocalDate localDate = LocalDate.parse("2018-06-05", dateTimeFormatter);
//
//		List<Repair> foundRepair= repairService.findByRepairDateEquals(localDate);
//		foundRepair.forEach(repair -> logger.info(repair.toString()));
//
//		logger.info("=============================");
//		logger.info("====Finding Repairs After Date====");
//		List<Repair> foundRepairs = repairService.findByRepairDateAfter(localDate);
//		foundRepairs.forEach(repair -> logger.info(repair.toString()));
//		logger.info("=============================");
//		logger.info("=============================");


//		logger.info("=============================");
//		logger.info("====Creating Owner====");
//		PropertyOwner po = new PropertyOwner(1241290L,
//											"dimitris",
//											"papapetrou",
//											"anastaseos 10",
//											2106522111,
//											"dimpap@gmail.com",
//											"123456",
//											PropertyTypeEnum.MAISONETTE,
//											RoleTypeEnum.DEFAULT);
//		propertyOwnerRepository.save(po);
//
//		logger.info("=============================");
//		logger.info("=============================");
//
//		logger.info("====Deleting Owner====");
//		propertyOwnerRepository.deleteById(po.getVat());
//		logger.info("=============================");
//		logger.info("=============================");

//		logger.info("=============================");
//		logger.info("====Finding Repairs Between Dates====");
//
//		LocalDate startDate = LocalDate.parse("2018-06-05", dateTimeFormatter);
//		LocalDate endDate = LocalDate.parse("2019-02-02", dateTimeFormatter);
//
//		List<Repair> foundRepairs2 = repairService.findByRepairDateBetween(startDate,endDate);
//		foundRepairs2.forEach(repair -> logger.info(repair.toString()));
//
//		logger.info("=============================");
//		logger.info("=============================");
//
//		logger.info("=============================");
//		logger.info("====Finding Repair By Owner Id====");
//
//
//		List<Repair> foundRepairs3 = repairService.findByPropertyOwnerId(1212123L);
//		foundRepairs3.forEach(repair -> logger.info(repair.toString()));
//
//		logger.info("=============================");
//		logger.info("=============================");

	}
}
