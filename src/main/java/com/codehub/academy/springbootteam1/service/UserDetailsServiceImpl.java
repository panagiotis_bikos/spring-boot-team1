package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.model.LoginResponse;
import com.codehub.academy.springbootteam1.repository.PropertyOwnerRepository;
import org.apache.naming.factory.SendMailFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    PropertyOwnerRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // here we would search into the repo for the user.
        // for not we are just going to send always a successful response.
        Optional<PropertyOwner> propertyOwnerOptional = userRepository.findByEmailContaining(username);
        if (propertyOwnerOptional.isEmpty()) {
            throw new UsernameNotFoundException("the user with email could not be found");
        }
        PropertyOwner propertyOwner = propertyOwnerOptional.get();

        return new LoginResponse(
                propertyOwner.getEmail(),
                propertyOwner.getPassword(),
                List.of(new SimpleGrantedAuthority(propertyOwner.getRole().name())),
                propertyOwner
        );


    }
}
