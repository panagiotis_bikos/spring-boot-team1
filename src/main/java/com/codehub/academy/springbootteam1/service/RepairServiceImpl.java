package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.mapper.RepairToRepairModel;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    private RepairRepository repairRepository;

    @Autowired
    private RepairToRepairModel mapper;

    //    SEARCH by Date (Equals, After & Between) and by Owner Id functionality

    @Override
    public List<RepairModel> findByRepairDateAfter(LocalDate repairDate) {
        return repairRepository
                .findByRepairDateIsAfter(repairDate)
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    @Override
    public List<RepairModel> findByRepairDateEquals(LocalDate repairDate) {
        return repairRepository
                .findByRepairDate(repairDate)
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    @Override
    public List<RepairModel> findByRepairDateBetween(LocalDate startDate, LocalDate endDate) {
        return repairRepository
                .findByRepairDateBetween(startDate, endDate)
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    @Override
    public List<RepairModel> findByPropertyOwnerId(Long id) {
        List<RepairModel> repairModels = new ArrayList<>();

        List<Repair> repairList = repairRepository.findByPropertyOwnerId(id);

        for (Repair repair : repairList) {
            RepairModel repairModel = mapper.mapToRepairModel(repair);
            repairModels.add(repairModel);
        }
        return repairModels;
    }

    @Override
    public List<RepairModel> findAll() {
        return repairRepository
                .findAll()
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    @Override
    public List<Repair> findByOwnerId(Long id) {
        return repairRepository.findByPropertyOwnerId(id);
    }

    @Override
    public RepairModel findById(Long id) {
        Repair repair = repairRepository.findById(id).get();
        return mapper.mapToRepairModel(repair);
    }

    // DELETE

    @Override
    public void deleteById(Long id) {
        repairRepository.deleteById(id);
    }

    // UPDATE

    @Override
    public Repair updateRepair(RepairModel repairModel) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Repair updatedRepair = repairRepository.findById(Long.valueOf(repairModel.getId())).get();

        updatedRepair.setDescription(repairModel.getDescription());
//        updatedRepair.setPropertyOwner(repairModel.getOwnerId());
        updatedRepair.setRepairAddress(repairModel.getAddress());
        updatedRepair.setRepairCost(Double.valueOf(repairModel.getCost()));
        updatedRepair.setRepairDate(LocalDate.parse(repairModel.getDate(), dateTimeFormatter));
        updatedRepair.setRepairStatus(repairModel.getStatusType());
        updatedRepair.setRepairType((repairModel.getRepairType()));
        updatedRepair.setRepairId(Long.valueOf(repairModel.getId()));
        return repairRepository.save(updatedRepair);
    }

    @Override
    public void createRepair(Repair repair) {
        repairRepository.save(repair);
    }

}