package com.codehub.academy.springbootteam1.enums;

public enum RepairTypeEnum {

    DEFAULT,
    PAINTING,
    INSULATION,
    FRAMING,
    PLUMBING,
    ELECTRONIC;
}
