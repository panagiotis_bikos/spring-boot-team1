package com.codehub.academy.springbootteam1.enums;

public enum RoleTypeEnum {

    USER,
    ADMIN;

}
