package com.codehub.academy.springbootteam1.repository;

import com.codehub.academy.springbootteam1.domain.Repair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RepairRepository extends JpaRepository<Repair,Long> {

    List<Repair> findByRepairDateIsAfter(LocalDate repairDate);

    List<Repair> findByRepairDate(LocalDate repairDate);

    List<Repair> findByRepairDateBetween(LocalDate startDate, LocalDate endDate);

    @Query("SELECT r FROM Repair r " +
            "INNER JOIN PropertyOwner p " +
            "ON r.propertyOwner.vat=p.vat " +
            "WHERE p.vat = ?1")
    List<Repair> findByPropertyOwnerId(Long id);
}
