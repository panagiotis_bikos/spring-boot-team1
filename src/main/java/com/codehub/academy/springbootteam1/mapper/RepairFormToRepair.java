package com.codehub.academy.springbootteam1.mapper;


import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.enums.RepairTypeEnum;
import com.codehub.academy.springbootteam1.forms.SearchRepairForm;
import com.codehub.academy.springbootteam1.service.PropertyOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class RepairFormToRepair {

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    public Repair toRepair(SearchRepairForm repairForm){
        Repair repair = new Repair();

        repair.setDescription(repairForm.getDescription());

        //getting existing property owner from database (if not should throw exception if id is not valid)
        PropertyOwner propertyOwner = propertyOwnerService.findById(Long.valueOf(repairForm.getOwnerId())).get();
        repair.setPropertyOwner(propertyOwner);

        repair.setRepairAddress(repairForm.getRepairAddress());
        repair.setRepairCost(Double.valueOf(repairForm.getRepairCost()));

        LocalDate date = LocalDate.parse(repairForm.getRepairDate(),DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        repair.setRepairDate(date);

        //also should check about id if it's valid search with repairService in DB if yes and positive then enter
        repair.setRepairId(Long.valueOf(repairForm.getRepairId()));
        repair.setRepairStatus(RepairStatusEnum.valueOf(repairForm.getRepairStatus()));
        repair.setRepairType(RepairTypeEnum.valueOf(repairForm.getRepairType()));
        return repair;

    }


}
