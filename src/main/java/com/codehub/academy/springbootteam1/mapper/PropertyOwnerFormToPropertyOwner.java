package com.codehub.academy.springbootteam1.mapper;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.enums.PropertyTypeEnum;
import com.codehub.academy.springbootteam1.enums.RoleTypeEnum;
import com.codehub.academy.springbootteam1.forms.SearchOwnerForm;
import org.springframework.stereotype.Component;

import static com.codehub.academy.springbootteam1.validators.SecurityConfig.getEncodedPassword;

@Component
public class PropertyOwnerFormToPropertyOwner {

    public PropertyOwner toPropertyOwner(SearchOwnerForm ownerForm){

        PropertyOwner owner = new PropertyOwner();

        owner.setAddress(ownerForm.getAddress());
        owner.setVat(Long.valueOf(ownerForm.getVat()));
        owner.setPropertyTypeEnum(PropertyTypeEnum.valueOf(ownerForm.getPropertyType()));

        //password should be encrypted here!
        String encodedPassword = getEncodedPassword(ownerForm.getPassword());
        owner.setPassword(encodedPassword);

        owner.setFirstName(ownerForm.getFirstName());
        owner.setLastName(ownerForm.getLastName());
        owner.setEmail(ownerForm.getEmail());
        owner.setPhoneNumber(ownerForm.getPhoneNumber());
        owner.setRole(RoleTypeEnum.valueOf(ownerForm.getRole()));

        return owner;
    }

}
